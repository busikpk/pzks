'use strict';

import {View} from './view';
import {readFile, writeToFile} from './tools';

const view = new View();

document.addEventListener('DOMContentLoaded', (event) => {
    initTabs();
    view.draw();
});

document.getElementById('new').onclick = event => view.clear();

document.getElementById('del').onclick = event => view.removeElement();

document.getElementById('validate').onclick = event => alert(`graph valid: ${view.validate()}`);

document.getElementById('add').onclick = event => {
    const value = prompt("Node value");
    if (value) {
        view.addNode(value);
    }
};

document.getElementById('link').onclick = event => {
    const weight = prompt("Edge weight");
    if (weight) {
        view.linkNodes(weight);
    }
};

document.getElementById('change').onclick = event => {
    const newValue = prompt("New element value");
    if (newValue) {
        view.changeElementContent(newValue);
    }
};

document.getElementById('load').onclick = e => document.getElementById('input-file').click();

document.getElementById('input-file').onchange = e => {
    readFile(e, (e) => {
        const data = JSON.parse(e.target.result);
        while (data.type != view.currentGraph.type) {
            view.checkout();
        }
        view.drawFromData(data.graph);
    });
};

document.getElementById('save').onclick = e => {
    writeToFile(view.currentGraph)
};

document.getElementById('generate').onclick = e => {
    const inputData = prompt("correlation/minWeight/maxWeight/count");
    const values = inputData.match(new RegExp('^(0.[0-9]+|1)\/([0-9]+)\/([0-9]+)\/([0-9]+)$'));
    if (values) {
        view.generateTaskGraph(values[1], values[2], values[3], values[4]);
    }
};

document.getElementById('model').onclick = e => {
    const s = document.getElementById('queueAlgorithm');
    const queueId = parseInt(s.options[s.selectedIndex].value);
    const s1 = document.getElementById('modelingAlgorithm');
    const modelingId = parseInt(s1.options[s1.selectedIndex].value);

    const processorSpeed = parseInt(document.getElementById('processorSpeed').value || 1);
    const linkSpeed = parseInt(document.getElementById('linkSpeed').value || 1);
    const channelsCount = parseInt(document.getElementById('channelsCount').value || 1);
    const isDuplex = document.getElementById('isDuplexTrue').checked || false;
    const packetSize = parseInt(document.getElementById('packetSize').value) || 0;
    const state = view.model(modelingId, queueId, processorSpeed, linkSpeed, channelsCount, isDuplex, packetSize);
    drawModelingTable(state);
};

document.getElementById('queues').onclick = e => {
    const [q1 = [], q2 = [], q3 = []] = view.getQueues();
    const str = (q) => q.map(o => `${o.node.id}(${o.value})`).join(', ');
    alert(`Alg2:\n${str(q1)}\nAlg4:\n${str(q2)}\nAlg7:\n${str(q3)}`);
};

function initTabs() {
    const tabListsAnchors = document.querySelectorAll('.tabs li a');
    const divs = document.querySelector('.tabs').getElementsByTagName('div');

    tabListsAnchors.forEach((t, i) => {

        if (t.classList.contains('active')) {
            divs[i].style.display = 'inline-block';
        }

        t.addEventListener('click', e => {

            for (let index = 0; index < divs.length; index++) {
                divs[index].style.display = 'none';
            }
            tabListsAnchors.forEach(tt => tt.classList.remove('active'));
            const clickedTab = e.target || e.srcElement;
            clickedTab.classList.add('active');
            let divToShow = clickedTab.getAttribute('href');
            document.querySelector(divToShow).style.display = 'inline-block';
            let divsToShow = document.querySelector(divToShow).getElementsByTagName('div');

            for (let index = 0; index < divsToShow.length; index++) {
                divsToShow[index].style.display = 'block';
            }

            for (const id of ['queues', 'generate']) {
                const generateBtn = document.getElementById(id);
                generateBtn.style.display = document.getElementById('task_a').classList.contains('active') ? 'inline-block' : 'none';
            }

            if(document.getElementById('task_a').classList.contains('active')) {
                view.checkoutToTask();
                view.draw();
            }
            if(document.getElementById('system_a').classList.contains('active')) {
                view.checkoutToSystem();
                view.draw();
            }
        });
    });
}

function drawModelingTable(state) {
    const table = document.getElementById('modeling_table');
    table.innerHTML = '';
    //create table header
    const header = document.createElement('tr');
    const thTime = document.createElement('th');
    const textTime = document.createTextNode('time');
    thTime.appendChild(textTime);
    header.appendChild(thTime);
    const columns = [];
    for(const [processorId, value] of state.processors) {
        const th = document.createElement('th');
        const text = document.createTextNode(processorId);
        th.appendChild(text);
        header.appendChild(th);
        columns.push(value);
    }
    for(const [linkId, value] of state.links) {
        const th = document.createElement('th');
        const text = document.createTextNode(linkId);
        th.appendChild(text);
        header.appendChild(th);
        columns.push(value);
    }
    table.appendChild(header);

    for (const key of columns[0].keys()) {
        const row = document.createElement('tr');
        const tdTime = document.createElement('td');
        const textTime = document.createTextNode(key);
        tdTime.appendChild(textTime);
        row.appendChild(tdTime);
        for (const column of columns) {
            const td = document.createElement('td');
            const text = document.createTextNode(column.get(key));
            td.appendChild(text);
            row.appendChild(td);
        }
        table.appendChild(row);
    }
}