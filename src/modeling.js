import {Node} from './models';

class Processor extends Node {

    constructor(node, speed, channelsCount) {
        super(node.value, node.edges);
        this.id = node.id;
        this.isBusy = false;
        this.speed = speed;
        this.channelsCount = channelsCount;
        this.listenProcessors = new Set();
        this.cache = [];
        this.queue = [];
        this.links = [];
        this.task = null;
        this.currentTaskWeight = 0;
    }

    execute() {
        if (this.task) {
            const processorCacheTasksIds = this.cache.map(p => p.id);
            const isProcessorHasAllParents = this.task.getParentNodes().reduce((acc, n) => {
                return acc && processorCacheTasksIds.includes(n.id);
            }, true);

            if (isProcessorHasAllParents) {
                this.task.value -= this.speed;
            }
        }
    }

    fillLinks() {
        const newQueue = [];
        while (this.queue.length) {
            const link = this.links.find(l => {
                if (this.queue[0].path.length === 1) {
                    return l.target.id === this.queue[0].path[0] || l.source.id === this.queue[0].path[0];
                } else {
                    const pathSlice = [this.queue[0].path[0], this.queue[0].path[1]];
                    return pathSlice.includes(l.target.id) && pathSlice.includes(l.source.id);
                }
            });
            if (link && link.setMessage(this.queue[0])) {
                this.queue[0].path.shift();
            } else {
                newQueue.push(this.queue[0]);
            }
            this.queue.shift();
        }
        this.queue = newQueue.concat(this.queue);
    }

    setTask(task) {
        this.task = task;
        this.isBusy = true;
        this.currentTaskWeight = task.value;
    }

    clear() {
        if (this.task && 0 >= this.task.value) {
            this.cache.push(this.task);
            this.task.done = true;
            this.task.value = this.currentTaskWeight;
            this.task = null;
            this.isBusy = false;
            this.currentTaskWeight = 0;
        }
    }

}

class Link {

    constructor(id, source, target, speed, isDuplex) {
        this.id = id;
        this.source = source;
        this.target = target;
        this.speed = speed;
        this.isDuplex = isDuplex;
        this.messageFromSource = null;
        this.messageFromSourceWeight = 0;
        this.messageFromTarget = null;
        this.messageFromTargetWeight = 0;
    }

    setMessage(message) {
        let isSet = false;
        const setMessageFromSource = () => {
            this.source.listenProcessors.add(this.target.id);
            this.target.listenProcessors.add(this.source.id);
            this.messageFromSource = message;
            this.messageFromSourceWeight = message.weight;
            isSet = true;
        };
        const setMessageFromTarget = () => {
            this.source.listenProcessors.add(this.target.id);
            this.target.listenProcessors.add(this.source.id);
            this.messageFromTarget = message;
            this.messageFromTargetWeight = message.weight;
            isSet = true;
        };
        const sourceChannelsLimit = Math.min(this.source.channelsCount, this.source.links.length);
        const targetChannelsLimit = Math.min(this.target.channelsCount, this.target.links.length);

        if (message.path[0] === this.source.id && !this.messageFromSource) {
            if (this.isDuplex) {
                if (this.source.listenProcessors.has(this.target.id) || (
                    this.source.listenProcessors.size < sourceChannelsLimit &&
                    this.target.listenProcessors.size < targetChannelsLimit)){
                        setMessageFromSource();
                }
            } else {
                if (this.source.listenProcessors.size < sourceChannelsLimit &&
                    this.target.listenProcessors.size < targetChannelsLimit && !this.messageFromTarget) {
                        setMessageFromSource();
                }
            }
        }
        if (message.path[0] === this.target.id && !this.messageFromTarget) {
            if (this.isDuplex) {
                if (this.target.listenProcessors.has(this.source.id) || (
                    this.source.listenProcessors.size < sourceChannelsLimit &&
                    this.target.listenProcessors.size < targetChannelsLimit)){
                        setMessageFromTarget();
                }
            } else {
                if (this.source.listenProcessors.size < sourceChannelsLimit &&
                    this.target.listenProcessors.size < targetChannelsLimit && !this.messageFromSource) {
                        setMessageFromTarget();
                }
            }

        }
        return isSet;
    }

    execute() {
        if (this.messageFromSource) {
            this.messageFromSourceWeight -= this.speed;
        }
        if (this.messageFromTarget) {
            this.messageFromTargetWeight -= this.speed;
        }
    }

    clear() {
        if (this.messageFromSource && this.messageFromSourceWeight <= 0) {
            if (this.messageFromSource.toId === this.target.id) {
                if (this.messageFromSource.data instanceof Node) {
                    this.target.cache.push(this.messageFromSource.data);
                } else {
                    if (this.messageFromSource.data instanceof Packet && this.messageFromSource.data.data) {
                        this.target.cache.push(this.messageFromSource.data.data);
                    }
                }
            } else {
                this.target.queue.push(this.messageFromSource);
            }
            this.messageFromSource = null;
            this.messageFromSourceWeight = 0;
        }
        if (this.messageFromTarget && this.messageFromTargetWeight <= 0) {
            if (this.messageFromTarget.toId === this.source.id) {
                if (this.messageFromTarget.data instanceof Node) {
                    this.source.cache.push(this.messageFromTarget.data);
                } else {
                    if (this.messageFromTarget.data instanceof Packet && this.messageFromTarget.data.data) {
                        this.source.cache.push(this.messageFromTarget.data.data);
                    }
                }
            } else {
                this.source.queue.push(this.messageFromTarget);
            }
            this.messageFromTarget = null;
            this.messageFromTargetWeight = 0;
        }
        if (!this.messageFromSource && !this.messageFromTarget) {
            this.source.listenProcessors.delete(this.target.id);
            this.target.listenProcessors.delete(this.source.id);
        }
    }

}

class Message {

    constructor(fromId, toId, weight, data, path) {
        this.fromId = fromId;
        this.toId = toId;
        this.weight = weight;
        this.data = data;
        this.path = path;
    }
}

class Packet {

    constructor(id, weight, data) {
        this.id = id;
        this.weight = weight;
        this.data = data;
    }
}

class State {

    constructor(processors, links) {
        this.processors = new Map();
        this.links = new Map();
        processors.forEach(p => {
            this.processors.set(p.id, new Map());
        });
        links.forEach(l => {
            this.links.set(l.id, new Map());
        });
    }
}

export class ExecutionSystem {

    constructor(systemGraph, tasksQueue, processorSpeed=1, linkSpeed=1, channelsCount=1, isDuplex=false, packetSize=0) {
        this.time = 0;
        this.done = [];
        this.links = [];
        this.processorSpeed = processorSpeed;
        this.linkSpeed = linkSpeed;
        this.channelsCount = channelsCount;
        this.isDuplex = isDuplex;
        this.packetSize = packetSize;
        this.systemGraph = systemGraph;

        this.processors = this.systemGraph.nodes.map(n => {
            n.value = parseInt(n.value);
            n.edges.forEach(e => e.weight = parseInt(e.weight));
            return new Processor(n, this.processorSpeed, this.channelsCount);
        });

        systemGraph.edges.forEach(e => {
            const source = this.processors.find(p => p.id === e.source.id);
            const target = this.processors.find(p => p.id === e.target.id);
            const link = new Link(e.id, source, target, this.linkSpeed, this.isDuplex);
            source.links.push(link);
            target.links.push(link);
            this.links.push(link);
        });

        this.queue = tasksQueue.map(n => {
            n.node.value = parseInt(n.node.value);
            n.node.edges.forEach(e => e.weight = parseInt(e.weight));
            n.node.done = false;
            return n.node;
        });
        this.processors.sort((a, b) => b.getAllNeighbors().length - a.getAllNeighbors().length);
        this.state = new State(this.processors, this.links);
    }

    tick() {
        this.execute();
        this.saveState();
        this.clear();
    }

    run() {
        const startQueueLength = this.queue.length;
        while (startQueueLength !== this.done.length) {
            this.setTasks();
            this.tick();
        }
        return this.state;
    }

    setTasks() {
        this.processors.forEach(p => {
            if (!p.isBusy) {
                const {task, newQueue} = this.queue.reduce((acc, t) => {
                    if (!acc.task) {
                        if (t.getParentNodes().every(n => n.done)) {
                            acc.task = t;
                        } else {
                            acc.newQueue.push(t);
                        }
                    } else {
                        acc.newQueue.push(t);
                    }
                    return acc;
                }, {task: null, newQueue: []});
                this.queue = newQueue;
                if (task) {
                    this.setParents(p, task);
                    p.setTask(task);
                }
            }
        });
    }

    setParents(targetProcessor, task) {
        const cacheTasksIds = targetProcessor.cache.map(t => t.id);
        const tasksForSending = task.getParentNodes().reduce((acc, n) => {
            if (!cacheTasksIds.includes(n.id)) {
                acc.push(n);
            }
            return acc;
        }, []);

        if (tasksForSending.length) {
            tasksForSending.forEach(t => {
                const processorsWithData = this.processors.reduce((acc, p) => {
                    if (p.cache.some(t1 => t1.id === t.id)) {
                        acc.push(p);
                    }
                    return acc;
                }, []);
                const {processor, path} = processorsWithData.reduce((acc, p) => {
                    const path = ExecutionSystem.shortestPaths(this.processors, p.id, targetProcessor.id);
                    if (!acc || path.length < acc.path.length) {
                        acc = {processor: p, path: path}
                    }
                    return acc;
                }, null);
                let msgWeight = task.edges.find(e => task.id === e.target.id && e.source.id === t.id).weight;
                if (!this.packetSize || this.packetSize > msgWeight) {
                    processor.queue.push(new Message(processor.id, targetProcessor.id, msgWeight, t, [...path]));
                } else {
                    const packets = [];
                    let i = 0;
                    while (msgWeight > this.packetSize) {
                        msgWeight -= this.packetSize;
                        const p = new Packet(`${t.id}.${i++}`, this.packetSize, null);
                        packets.push(new Message(processor.id, targetProcessor.id, this.packetSize, p, [...path]));
                    }
                    const p = new Packet(`${t.id}.${i}`, msgWeight, t);
                    packets.push(new Message(processor.id, targetProcessor.id, msgWeight, p, [...path]));
                    processor.queue = processor.queue.concat(packets);
                }
            });
        }
    }

    execute() {
        ++this.time;
        this.processors.forEach(p => p.fillLinks());
        this.processors.forEach(p => p.execute());
        this.links.forEach(l => l.execute());
    }

    clear() {
        this.processors.forEach(p => {
            if (p.task && 0 >= p.task.value) {
                this.done.push(p.task);
                p.clear();
            }
        });
        this.links.forEach(l => l.clear());
    }

    saveState() {
        this.processors.forEach(p => {
            const flags = [];
            if (p.task) {
                flags.push(p.task.id);
                const cacheTasksIds = p.cache.map(t => t.id);
                const waitingTasks = p.task.getParentNodes().reduce((acc, n) => {
                    if (!cacheTasksIds.includes(n.id)) {
                        acc.push(n);
                    }
                    return acc;
                }, []);
                if (waitingTasks.length) {
                    flags.push('w');
                }
            }
            this.state.processors.get(p.id).set(this.time, flags);
        });
        this.links.forEach(l => {
            const flags = [];
            if (l.messageFromSource) {
                flags.push(`${l.messageFromSource.fromId}=>${l.messageFromSource.toId}[${l.messageFromSource.data.id}]`);
            }
            if (l.messageFromTarget) {
                flags.push(`${l.messageFromTarget.fromId}=>${l.messageFromTarget.toId}[${l.messageFromTarget.data.id}]`);
            }
            this.state.links.get(l.id).set(this.time, flags);
        });
    }

    static shortestPaths(nodes, fromId, toId) {
        //Dijkstra
        const d = new Map();
        const v = new Map();
        const p = new Map();
        const inf = 99999;
        nodes.forEach(n => {
            d.set(n.id, fromId === n.id ? 0 : inf);
            v.set(n.id, false);
        });

        for (let i in nodes) {
            const n = nodes.reduce((acc, node) => {
                return !v.get(node.id) && (acc === null || d.get(node.id) < d.get(acc.id)) ? node : acc
            }, null);

            if (d.get(n) === inf) break;

            v.set(n.id, true);

            n.edges.forEach(e => {
                const key = n.id === e.target.id ? e.source.id : e.target.id;
                if (d.get(n.id) + e.weight < d.get(key)) {
                    d.set(key, d.get(n.id) + e.weight);
                    p.set(key, n.id);
                }
            });
        }
        const path = [];
        while (fromId !== toId) {
            path.push(toId);
            toId = p.get(toId);
        }
        path.push(fromId);
        path.reverse();
        return path;
    }

    static createExecutionSystem(systemGraph, queue, processorSpeed=1, linkSpeed=1, channelsCount=1, isDuplex=false, packetSize=0) {
        const executionSystem = new ExecutionSystem(systemGraph, [], processorSpeed, linkSpeed, channelsCount, isDuplex, packetSize);
        executionSystem.queue = queue.map(n => n.getCopy());
        return executionSystem;
    }
}

export class ExecutionSystem6 extends ExecutionSystem{

    constructor(systemGraph, tasksQueue, processorSpeed=1, linkSpeed=1, channelsCount=1, isDuplex=false, packetSize=0) {
        super(systemGraph, tasksQueue, processorSpeed, linkSpeed, channelsCount, isDuplex, packetSize);
    }

    setTasks() {
        let freeProcessors = this.processors.filter(p => !p.isBusy);
        while (this.queue.some(t => t.getParentNodes().every(n => n.done)) && freeProcessors.length) {
            const {task, newQueue} = this.queue.reduce((acc, t) => {
                if (!acc.task) {
                    if (!t.getParentNodes().filter(n => !n.done).length) {
                        acc.task = t;
                    } else {
                        acc.newQueue.push(t);
                    }
                } else {
                    acc.newQueue.push(t);
                }
                return acc;
            }, {task: null, newQueue: []});
            this.queue = newQueue;
            if (task) {
                const fakeTask = task.getCopy();
                const processor = this.getProcessor(freeProcessors, fakeTask);
                this.setParents(processor, task);
                processor.setTask(task);
                freeProcessors = freeProcessors.filter(p => p.id !== processor.id);
            }
        }
    }

    getProcessor(processors, task) {
        const weightsMap = processors.map(p => {
            const executionSystem = ExecutionSystem.createExecutionSystem(this.systemGraph, this.queue, this.processorSpeed,
                this.linkSpeed, this.channelsCount, this.isDuplex);

            executionSystem.processors.forEach(fakeProcessor => {
                const originalProcessor = this.processors.find(p1 => p1.id === fakeProcessor.id);
                fakeProcessor.cache = [...originalProcessor.cache];
                fakeProcessor.queue = originalProcessor.queue.map(m => new Message(m.fromId, m.toId, m.weight, m.data, [...m.path]));
                fakeProcessor.task = originalProcessor.task ? originalProcessor.task.getCopy() : null;
                fakeProcessor.isBusy = originalProcessor.isBusy;
                fakeProcessor.currentTaskWeight = originalProcessor.currentTaskWeight;
                fakeProcessor.listenProcessors = new Set(originalProcessor.listenProcessors);
            });

            const currentProcessor = executionSystem.processors.find(fakeProcessor => fakeProcessor.id === p.id);
            executionSystem.setParents(currentProcessor, task);
            const needExecution = () => {
                return task.getParentNodes().some(n => {
                    return !currentProcessor.cache.map(t => t.id).includes(n.id);
                });
            };
            while (needExecution()) {
                executionSystem.tick();
            }

            return {weight: executionSystem.time, processor: p};
        });

        const {processor} = weightsMap.reduce((acc, o) => {
            if (!acc || o.weight < acc.weight) {
                acc = {processor: o.processor, weight: o.weight};
            }
            return acc;
        }, null);

        return processor;
    }

}