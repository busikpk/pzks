/**
 * Created by busik on 20.02.17.
 */

export const TASK_TYPE = 0;
export const SYSTEM_TYPE = 1;

const ID = (function *() {
    let index = 1;
    while (true) {
        yield index++;
    }
})();

export class Node {

    constructor(value = null, edges = []) {
        this.id = ID.next().value;
        this.value = value;
        this.edges = edges;
        this.done = false;
    }

    toJSON() {
        return {
            id: this.id,
            name: this.value,
            label: `${this.value}[${this.id}]`
        }
    }

    getChildNodes() {
        return this.edges.reduce((acc, e) => {
            if (e.source.id == this.id) {
                acc.push(e.target);
            }
            return acc;
        }, []);
    }

    getParentNodes() {
        return this.edges.reduce((acc, e) => {
            if (e.target.id == this.id) {
                acc.push(e.source);
            }
            return acc;
        }, []);
    }

    getAllNeighbors() {
        return this.getParentNodes().concat(this.getChildNodes());
    }

    getCopy() {
        return Object.assign(Object.create(Object.getPrototypeOf(this)), this);
    }
}

export class Edge {

    constructor(source = null, target = null, weight = 1, directed = true) {
        this.id = ID.next().value;
        this.source = source;
        this.target = target;
        this.weight = weight;
        this.directed = directed
    }

    toJSON() {
        return {
            id: this.id,
            source: this.source.id,
            target: this.target.id,
            weight: this.weight,
            label: `${this.weight}[${this.id}]`,
            directed: this.directed
        }
    }
}


export class Graph {

    constructor(type = TASK_TYPE, nodes = [], edges = []) {
        this.type = type;
        this.nodes = nodes;
        this.edges = edges;
    }

    validate() {
        throw new Error('Not Implemented, abstract method validate');
    }

    addNode(value) {
        const node = new Node(value);
        this.nodes.push(node);
        return node;
    }

    addEdge(sourceId, targetId, weight) {
        if (sourceId == targetId) {
            console.error('sourceId eq targetId', sourceId, targetId);
            return null;
        }

        const [source, target] = this.nodes.reduce((acc, node) => {
            if (sourceId == node.id) acc[0] = node;
            if (targetId == node.id) acc[1] = node;
            return acc;
        }, [null, null]);

        if (source && target && source.edges.filter(e => e.target.id != targetId).length == source.edges.length) {
            const edge = new Edge(source, target, weight, this.type == TASK_TYPE);
            source.edges.push(edge);
            target.edges.push(edge);
            this.edges.push(edge);
            return edge;

        } else {
            console.error(`bad args ${sourceId} ${targetId}`);
            return null;
        }
    }

    remove(elementId) {
        this.nodes = this.nodes.reduce((acc, n) => {
            if (n.id == elementId) {
                return acc;
            } else {
                n.edges = n.edges.filter(e => {
                    return !(e.id == elementId || e.target.id == elementId || e.source.id == elementId)
                });
                acc.push(n);
                return acc;
            }
        }, []);

        this.edges = this.edges.filter(e => {
            return !(e.id == elementId || e.target.id == elementId || e.source.id == elementId)
        });
    }

    changeElementValue(elementId, newElementValue, isNode) {
        if (isNode) {
            this.nodes.forEach(n => n.value = n.id == elementId ? newElementValue : n.value);
        } else {
            this.edges.forEach(e => e.weight = e.id == elementId ? newElementValue : e.weight);
        }
    }

    toJSON() {
        const nodes = this.nodes.map(node => ({data: node.toJSON()}));
        const edges = this.edges.map(edge => ({data: edge.toJSON()}));
        return [...nodes, ...edges];
    }

    fromJSON(graphJson) {
        const nodes = new Map();
        const rawEdges = [];

        graphJson.forEach(element => {
            const item = element.data;
            if (!item.hasOwnProperty('source') && !item.hasOwnProperty('target')) {
                const node = new Node(item.name);
                node.id = item.id;
                nodes.set(item.id, node);
            } else {
                rawEdges.push(item);
            }
        });

        this.edges = rawEdges.map(edge => {
            const source = nodes.get(edge.source);
            const target = nodes.get(edge.target);
            return new Edge(source, target, edge.weight, edge.directed);
        });

        const newNodes = [];

        for (const node of nodes.values()) {
            node.edges = this.edges.filter(e => e.source.id == node.id || e.target.id == node.id);
            newNodes.push(node);
        }

        this.nodes = newNodes;
    }

    getStyle() {
        return [
            {
                selector: 'node',
                style: {
                    label: 'data(label)',
                    'text-valign': 'center',
                    'height': 30,
                    'width': 30,
                }
            },
            {
                selector: 'edge',
                css: {
                    'target-arrow-shape': this.type == TASK_TYPE ? 'triangle' : '',
                    'curve-style': 'bezier',
                    'label': 'data(label)',
                    'line-color': '#ff4040'
                }
            }
        ]
    }
}

export class SystemGraph extends Graph {

    constructor(nodes = [], edges = []) {
        super(SYSTEM_TYPE, nodes, edges);
    }

    validate() {
        const nodes = this.nodes;

        function dfs(node, visitedIds) {
            visitedIds.push(node.id);

            for (const neighbor of node.getAllNeighbors()) {
                if (!visitedIds.includes(neighbor.id)) {
                    dfs(neighbor, visitedIds);
                }
            }
            return visitedIds;
        }

        return dfs(nodes[0], []).length == nodes.length;
    }
}

export class TaskGraph extends Graph {

    constructor(nodes = [], edges = []) {
        super(TASK_TYPE, nodes, edges);
    }

    validate() {
        const nodes = this.nodes;

        function hasLoops(node, visitedIds) {
            const currentNodeId = node.id;

            if (visitedIds.includes(currentNodeId)) {
                return true;
            }

            visitedIds.push(currentNodeId);

            for (const neighbor of node.getChildNodes()) {
                if (hasLoops(neighbor, visitedIds.map(id => id))) {
                    return true;
                }
            }
            return false;
        }

        return nodes.reduce((acc, n) => hasLoops(n, []) ? [true] : acc, []).length == 0;
    }

    static generate(correlation, minNodeWeight, maxNodeWeight, nodesCount) {
        const nodes = [];
        const edges = [];
        const randValueBetween = (l, r) => {
            let weight = Math.floor(Math.random() * (r - l + 1) + l);
            while (!weight) {
                weight = randValueBetween(l, r);
            }
            return weight;
        };
        const index = (l, r) => Math.floor(Math.random() * (r - l)) + l;

        while (nodesCount > nodes.length) {
            nodes.push(new Node(randValueBetween(minNodeWeight, maxNodeWeight)));
        }

        let l = nodes.reduce((acc, n) => acc + n.value, 0) * (1 / correlation - 1);
        const edgesWeights = [];
        const l0 = l;
        while (l > 0) {
            const weight = randValueBetween(1, Math.min(l0/10, l));
            edgesWeights.push(weight);
            l -= weight
        }

        let executions = 0;

        while (edgesWeights.length > 0) {

            if (executions++ > 100000) {
                console.log('to more executions');
                return null;
            }

            const source = nodes[index(0, nodes.length)];
            const target = nodes[index(0, nodes.length)];

            if (source.edges.filter(e => e.target.id == target.id).length > 0 || source.id == target.id) {
                continue;
            }

            const weight = edgesWeights.pop();
            const edge = new Edge(source, target, weight);

            source.edges.push(edge);
            target.edges.push(edge);
            edges.push(edge);

            if (!new TaskGraph(nodes, edges).validate()) {
                source.edges.pop();
                target.edges.pop();
                edges.pop();
                edgesWeights.push(weight);
            }
        }

        return new TaskGraph(nodes, edges);

    }
}
