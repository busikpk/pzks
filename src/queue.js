export class Queue {

    constructor(graph) {
        this.graph = graph;
    }

    getCriticalPaths() {
        function pathFinder(node, neighborsFn, pathChooseFn, path) {
            path.push(node);
            const ps = [path];

            for (const neighbor of neighborsFn(node)) {
                const p = pathFinder(neighbor, neighborsFn, pathChooseFn, []);
                ps.push([...path].concat(p));
            }

            return pathChooseFn(ps);
        }

        const nodes = this.graph.nodes;
        const pathsForEnding = new Map();
        const pathsForBeginnings = new Map();
        const pathsForEndingByNodesCount = new Map();
        const pathsForBeginningsByNodesCount = new Map();
        const weightedPathFn = (ps) => {
            const pfn = (t) => t.reduce((acc, n) => acc + parseInt(n.value), 0);
            return ps.reduce((res, p) => {
                return pfn(p) > pfn(res) ? p : res;
            }, []);
        };
        const nodesCountPathFn = (ps) => ps.reduce((res, p) => p.length > res.length ? p : res, []);

        nodes.forEach(n => {
            const path = pathFinder(n, (node) => node.getParentNodes(), weightedPathFn, []);
            path.shift();
            const path1 = pathFinder(n, (node) => node.getParentNodes(), nodesCountPathFn, []);
            path1.shift();

            pathsForBeginnings.set(n, path);
            pathsForEnding.set(n, pathFinder(n, (node) => node.getChildNodes(), weightedPathFn, []));
            pathsForEndingByNodesCount.set(n, pathFinder(n, (node) => node.getChildNodes(), nodesCountPathFn, []));
            pathsForBeginningsByNodesCount.set(n, path1);
        });

        return {pathsForEnding, pathsForBeginnings, pathsForEndingByNodesCount, pathsForBeginningsByNodesCount};
    }

    static alg2(paths) {
        let graphCriticalPathWeight = 0;
        const queue = [];

        for (const [node, path] of paths.pathsForEnding.entries()) {
            const currentPathWeight = path.reduce((acc, n) => acc + parseInt(n.value), 0);
            if (currentPathWeight > graphCriticalPathWeight) {
                graphCriticalPathWeight = currentPathWeight;
            }
        }

        for (const [node, path] of paths.pathsForEnding.entries()) {
            const currentPathWeight = path.reduce((acc, n) => acc + parseInt(n.value), 0);
            const currentPathWeightForBeginning = paths.pathsForBeginnings.get(node).reduce((acc, n) => acc + parseInt(n.value), 0);
            queue.push({
                node: node,
                value: graphCriticalPathWeight - currentPathWeight - currentPathWeightForBeginning
            });
        }

        queue.sort((a, b) => a.value - b.value);

        return queue;
    }

    static alg4(paths) {
        const queue = [];

        paths.pathsForEndingByNodesCount.forEach((path, node) => {
            queue.push({node: node, value: [path.length, node.getAllNeighbors().length]});
        });

        queue.sort((a, b) => {
            if (a.value[0] == b.value[0]) {
                if (a.value[1] > b.value[1]) {
                    return -1;
                } else {
                    return 1;
                }
            } else {
                if (a.value[0] > b.value[0]) {
                    return -1;
                } else {
                    return 1;
                }
            }
        });

        return queue;
    }

    static alg7(paths) {
        const queue = [];

        paths.pathsForBeginningsByNodesCount.forEach((path, node) => {
            queue.push({node: node, value: [path.length, node.getAllNeighbors().length]});
        });

        queue.sort((a, b) => {
            if (a.value[0] == b.value[0]) {
                if (a.value[1] > b.value[1]) {
                    return -1;
                } else {
                    return 1;
                }
            } else {
                if (a.value[0] > b.value[0]) {
                    return 1;
                } else {
                    return -1;
                }
            }
        });

        return queue;
    }
}