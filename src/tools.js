export function readFile(e, fn) {
    const file = e.target.files[0];
    if (!file) {
        return null;
    }
    const reader = new FileReader();
    reader.onload = fn;
    reader.readAsText(file);
}

export function writeToFile(graphObj) {
    const blob = new Blob([JSON.stringify({type: graphObj.type, graph: graphObj.toJSON()})]);
    const link = document.createElement('a');
    link.href = window.URL.createObjectURL(blob);
    link.download = "graph.txt";
    link.click();
}