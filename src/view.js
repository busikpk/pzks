/**
 * Created by busik on 21.02.17.
 */

import {SystemGraph, TaskGraph, TASK_TYPE} from './models';
import {Queue} from './queue';
import {ExecutionSystem, ExecutionSystem6} from './modeling';

export class View {

    constructor() {
        this.taskGraph = new TaskGraph();
        this.systemGraph = new SystemGraph();
        this.currentGraph = this.taskGraph;
        this.lastSelectedElement = null;
        this.currentSelectedElement = null;
    }

    validate() {
        return this.currentGraph.validate();
    }

    addNode(value) {
        const node = this.currentGraph.addNode(value);
        const rand = () => Math.ceil(Math.random() * 100);
        this.graphView.add({data: node.toJSON(), position: {x: 150 + rand(), y: 150 + rand()}});
    }

    linkNodes(weight) {
        if (this.lastSelectedElement && this.currentSelectedElement) {
            const edge = this.currentGraph.addEdge(this.lastSelectedElement, this.currentSelectedElement, weight);
            if (edge) {
                this.graphView.add({data: edge.toJSON()});
            }
        }
    }

    removeElement() {
        this.graphView.remove(this.graphView.getElementById(this.currentSelectedElement));
        this.currentGraph.remove(parseInt(this.currentSelectedElement));
    }

    clear() {
        this.currentGraph.nodes = [];
        this.currentGraph.edges = [];
        this.graphView.destroy();
        this.draw();
    }

    checkoutToSystem() {
        this.currentGraph = this.systemGraph;
    }

    checkoutToTask() {
        this.currentGraph = this.taskGraph;
    }

    draw() {
        if (this.graphView) this.graphView.destroy();

        this.graphView = require('cytoscape')({
            container: document.getElementById('cy' + this.currentGraph.type.toString()),
            elements: this.currentGraph.toJSON(),
            style: this.currentGraph.getStyle(),
            layout: {name: 'circle'}
        });
        this.graphView.zoom(1);
        this.graphView.on('tap', (e) => {
            if (e.cyTarget === this.graphView) {
                this.lastSelectedElement = null;
                this.currentSelectedElement = null;
            }
        });

        this.graphView.on('tap', 'node', (e) => {
            this.lastSelectedElement = this.currentSelectedElement;
            this.currentSelectedElement = e.cyTarget.id()
        });


        this.graphView.on('tap', 'edge', (e) => {
            this.lastSelectedElement = null;
            this.currentSelectedElement = e.cyTarget.id()
        });
    }

    drawFromData(graphJson) {
        this.currentGraph.fromJSON(graphJson);
        this.draw();
    }

    changeElementContent(newValue) {
        if (newValue) {
            const element = this.graphView.getElementById(this.currentSelectedElement);
            this.currentGraph.changeElementValue(parseInt(this.currentSelectedElement), newValue, element.isNode());
            element.data("label", `${newValue}[${this.currentSelectedElement}]`);
        }
    }

    generateTaskGraph(correlation, minNodeWeight, maxNodeWeight, nodesCount) {
        const res = TaskGraph.generate(correlation, minNodeWeight, maxNodeWeight, nodesCount);
        if (res) {
            this.currentGraph = res;
            this.taskGraph = res;
            this.draw();
        }
    }

    getQueues() {
        const criticalPaths = new Queue(this.taskGraph).getCriticalPaths();
        return [Queue.alg2(criticalPaths), Queue.alg4(criticalPaths), Queue.alg7(criticalPaths)];
    }

    model(modelingId, queueId, processorSpeed, linkSpeed, channelsCount, isDuplex, packetSize) {
        const criticalPaths = new Queue(this.taskGraph).getCriticalPaths();
        let queue = [];
        switch (queueId) {
            case 2: queue = Queue.alg2(criticalPaths); break;
            case 4: queue = Queue.alg4(criticalPaths); break;
            case 7: queue = Queue.alg7(criticalPaths); break;
            default:
                console.error('unknown queue id');
        }
        let system = null;
        switch (modelingId) {
            case 6:
                system = new ExecutionSystem6(this.systemGraph, queue, processorSpeed, linkSpeed, channelsCount, isDuplex, packetSize);
                break;
            default:
                system = new ExecutionSystem(this.systemGraph, queue, processorSpeed, linkSpeed, channelsCount, isDuplex, packetSize);
        }
        const state = system.run();
        console.log(state);
        return state;
    }

}