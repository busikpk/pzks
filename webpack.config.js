'use strict';

module.exports = {
    devtool: "source-map",
    entry: "./src/app",
    output: {
        filename: "./build/bundle.js"
    }
};